def calc_pos(seq, i, j):
	return int(j*(len(seq) + 1) + i)

def pairwise_scores(seq_a, seq_b, delta):
    best_scores = [0] * (len(seq_a)+1) * (len(seq_b)+1)
    best_scores[calc_pos(seq_a, len(seq_a), len(seq_b))] = 0
    
    for j in range(len(seq_b)-1, -1, -1):
        best_scores[calc_pos(seq_a, len(seq_a), j)] = best_scores[calc_pos(seq_a, len(seq_a), j+1)] + delta['-'][seq_b[j]]
        
    for i in range(len(seq_a)-1, -1, -1):
        best_scores[calc_pos(seq_a, i, len(seq_b))] = best_scores[calc_pos(seq_a, i+1, len(seq_b))] + delta['-'][seq_a[i]]
        for j in range(len(seq_b)-1, -1, -1):
            cc = best_scores[calc_pos(seq_a, i+1, j+1)] + delta[seq_a[i]][seq_b[j]]
            c_ = best_scores[calc_pos(seq_a, i+1, j)] + delta[seq_a[i]]['-']
            _c = best_scores[calc_pos(seq_a, i, j+1)] + delta['-'][seq_b[j]]
            
            vals = [cc, c_, _c]
            best_scores[calc_pos(seq_a, i, j)] = min(vals)
            
    return best_scores

def get_carillo_lipman_matrix(seqs, delta):
	dim = len(seqs)
	scores = [0]*dim*dim
	
	for i in range(0, dim):
		for j in range(i+1, dim):
			scores[i*dim + j] = pairwise_scores(seqs[i], seqs[j], delta)
			
	return scores

def get_score(seqs, pos, scores):
    dim = len(seqs)
    total_score = 0
    
    for i in range(dim):
        for j in range(i+1, dim):
            total_score += scores[i * dim + j][calc_pos(seqs[i], pos[i], pos[j])]
            
    return total_score
	

