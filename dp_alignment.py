import math
import copy
import sys
import scoring
import time

start_time = time.time()
f = open(sys.argv[1], 'r')
seqs = [line.rstrip() for line in f.readlines()]
f.close()
delta = scoring.read_blosum62(sys.argv[2])

cumulative_len = [1]
lens = []
total_size = 1
for seq in seqs:
    total_size = total_size * (len(seq)+1)
    cumulative_len.append(total_size)
    lens.append(len(seq))

dims = len(seqs)

best_scores = [0]*total_size
parents = [0]*total_size

def convert(pos):
    right_pos = 0
    for i in range(0, len(lens)):
        right_pos += (pos[i] * cumulative_len[i])

    return right_pos

def get_next_pos(pos):
    d = 0
    while(d < len(lens)):
        if(pos[d] == lens[d]):
            pos[d] = 0
            d += 1
        else:
            pos[d] += 1
            break


first_pos = [0]*len(seqs)
last_pos = copy.deepcopy(lens)
pos = copy.deepcopy(first_pos)

while (pos != last_pos):
    get_next_pos(pos)
    pos_idx = convert(pos)
    best_scores[pos_idx] = float("inf")
    parents[pos_idx] = 0
    
    chars_base = []
    for d in range(0, dims):
        if(pos[d] > 0):
            chars_base.append(seqs[d][pos[d]-1])
        else:
            chars_base.append('')

    for perm in (1, math.pow(2,dims)):
        neighbors = copy.deepcopy(pos)
        chars = copy.deepcopy(chars_base)
        valid = True
        
        for d in range(0, dims):
            if (perm >= math.pow(2,d)):
                if (neighbors[d] == 0):
                    valid = False
                    break
                else:
                    neighbors[d] = neighbors[d] - 1
            else:
                chars[d] = '-'

        if (valid == True):
            if(best_scores[convert(neighbors)] == float("inf")):
                nei_score = scoring.sp_score(chars, delta)
            else:
                nei_score = best_scores[convert(neighbors)] + scoring.sp_score(chars, delta)  
            if(best_scores[pos_idx] > nei_score):
                best_scores[pos_idx] = nei_score
                parents[pos_idx] = perm

best_score = best_scores[convert(last_pos)]
alignment = ['']*len(seqs)

while (pos != first_pos):
    perm = parents[convert(pos)]
    
    if(perm == 0):
        break
        
    for d in range(0, dims):
        if(perm >= math.pow(2,d)):
            if(pos[d] <= 0):
                continue
            alignment[d] += seqs[d][pos[d]-1]
            pos[d] = pos[d]-1

        else:
            alignment[d] += '-'

end_time = time.time()

for align in alignment:
    print(align[::-1])

score_print = "The score for the alignment:" + str(best_score)
print(score_print)

print("time elapsed: ")
print(end_time - start_time)