def read_blosum62(path):
    delta = {}
    with open(path, 'r') as f:
        lines = [line.rstrip() for line in f.readlines()][6:]
        
    characters = lines[0].split()
    for i in range(1, len(lines)):
        comp = lines[i].split()
        
        if(comp[0] not in delta):
            delta[comp[0]] = {}
            
        for j in range(1, len(comp)):
            delta[comp[0]][characters[j-1]] = int(comp[j])
            
            if(characters[j-1] not in delta):
                delta[characters[j-1]] = {}
            delta[characters[j-1]][comp[0]] = int(comp[j])
            
    return delta

def sp_score(chars, delta):
    total = 0
    for i in range(0, len(chars)):
        for j in range(i+1, len(chars)):
            total += delta[chars[i]][chars[j]]
            
    return total