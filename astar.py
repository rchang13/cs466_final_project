import carillo_lipman
import scoring
import heapq
import math
import copy
import sys
import time

class Node:
    def __init__(self, index, f, g, p):
        self.index = index
        self.f = f
        self.g = g
        self.parent = p
        

    def __lt__(self, other):
        if((self.f + self.g) == (other.f + other.g)):
            if(self.index == other.index):
                if(self.parent < other.parent):
                    return True
                else:
                    return False
            elif(self.index < other.index):
                return True
            else:
                return False
        elif((self.f + self.g) < (other.f + other.g)):
            return True
        else:
            return False

      
    def __cmp__(self, obj):
        if((self.f + self.g) == (obj.f + obj.g)):
            if(self.index == obj.index):
                if(self.parent > obj.parent):
                    return 1
                elif(self.parent < obj.parent):
                    return -1
                else:
                    return 0
            elif(self.index < obj.index):
                return -1
            else:
                return 1
        elif((self.f + self.g) < (obj.f + obj.g)):
            return -1
        else:
            return 1
        
start_time = time.time()

f = open(sys.argv[1], 'r')
seqs = [line.rstrip() for line in f.readlines()]
f.close()

delta = scoring.read_blosum62(sys.argv[2])
carillo_matrix = carillo_lipman.get_carillo_lipman_matrix(seqs, delta)

dim = len(seqs)
cumulative_len = [1]
lens = []
total_size = 1
for seq in seqs:
    total_size = total_size * (len(seq)+1)
    cumulative_len.append(total_size)
    lens.append(len(seq))
    
def convert_back(idx):
    resized_pos = [0]*len(lens)
    for d in range(len(lens)-1, -1, -1):
        resized_pos[d] = idx/cumulative_len[d]
        idx %= cumulative_len[d]
        
    return resized_pos

def get_backward_neighbor_by_index(index, perm, chars, seqs):
    neighbor = index
    for d in range(len(lens)-1, -1, -1):
        if(perm == math.pow(2,d)):
            if(cumulative_len[d] > index):
                return False, 0
            else:
                neighbor -= cumulative_len[d]
                pos_d = index//cumulative_len[d]
                chars[d] = seqs[d][pos_d-1]
                if pos_d < 0:
                    chars[d] = ''
        else:
            chars[d] = '-'
        
        index %= cumulative_len[d]
        
    return True, neighbor

def get_forward_neighbor_by_index(index, perm, chars, seqs):
    neighbor = index
    for d in range(len(lens)-1, -1, -1):
        if(perm >= math.pow(2,d)):
            pos_d = index//cumulative_len[d]
            if(pos_d >= lens[d]):
                return False, 0
            else:
                neighbor += cumulative_len[d]
                chars[d] = seqs[d][pos_d]
        else:
            chars[d] = '-'
        index %= cumulative_len[d]
        
    return True, neighbor

openset = []
heapq.heapify(openset)
heapq.heappush(openset,Node(0,0,0,0)) 

parents = dict()

first_pos = [0]*len(seqs)
last_pos = lens
dims = len(seqs)

first_idx = 0
last_idx = total_size-1
visited_num = 0
visited = set()

best_score = float("inf")

chars = [''] * dims
while len(openset) > 0:
    node = heapq.heappop(openset)
    
    visited.add(node.index)
    visited_num += 1
    
    if(node.index in parents):
        continue
    parents[node.index] = node.parent
    
    if(node.index == last_idx):
        best_score = node.f

    for perm in (1, math.pow(2,dims)):
        valid, neighbor_idx = get_forward_neighbor_by_index(node.index, perm, chars, seqs)
        
        if((valid == False) or (neighbor_idx in parents)):
            continue
        
        neighbor_pos = convert_back(neighbor_idx)
        cl_score = carillo_lipman.get_score(seqs, neighbor_pos, carillo_matrix)
        sp_score = scoring.sp_score(chars, delta)
        
        heapq.heappush(openset,Node(neighbor_idx, node.f+sp_score, cl_score, perm)) 
    
alignment = [[0]*len(seqs)]

index = last_idx
while(index != first_idx):
    perm = parents[index]
    
    valid, index = get_backward_neighbor_by_index(index, perm, chars, seqs)
    
    if(valid == False):
        continue
        
    for d in range(0, dims):
        alignment[d] += chars[d]
        
end_time = time.time()

for align in alignment:
    print(align[::-1])
    
score_print = "The score for the alignment:" + str(best_score)
print(score_print)

print("time elapsed: ")
print(end_time - start_time)